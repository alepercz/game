# Game

Application use SWAPI API to select random people or starships and render their details to see who would win based on a common attribute.

- People have mass and starships have crew.
- A person with greater mass wins, a starship with more crew wins.
- The app render the attributes from the resource in a simple web page that allows you to 'play' the game.
- Once two cards are displayed the app declare one of the cards a winner based on the higher common attribute.
- Having displayed the winning card, the user is able to play again using an action button that repeats the same request.
- The app has score counter. There are two players, left and right, and score couter shows how many times each side has won.
- The app has option to select which resource to play against (people or starships)
- The app includes unit and integration tests
- The app based on components from Material-UI library

SWAPI API: https://swapi.dev/documentation
